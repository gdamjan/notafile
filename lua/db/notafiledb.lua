local lfs = require 'lfs'
local md5 = require 'md5'
local ldb = require 'leveldb'
local mimetypes = require 'mimetypes'

local md5_db = ldb.open(
  'md5.db',
  { create_if_missing = true, error_if_exists = false }
)

local dir = lfs.currentdir()
if arg[1] and lfs.attributes(arg[1]) then
  dir = arg[1]
end

local function scandir(path, cb)
  for file in lfs.dir(path) do
    if file ~= '.' and file ~= '.' and file:sub(0, 1) ~= '.' then
      local f = path .. '/' ..file
      local attrs = lfs.attributes(f)

      if attrs then
        cb(f, attrs)

        if attrs.mode == 'directory' then scandir(f, cb) end
      end
    end
  end
end

scandir(
  dir,
  function(f, attrs)
    if attrs == nil or attrs.mode == 'directory' then
      return
    end

    local p_mtime = md5_db:get('mtime:' .. f)
    local mtime = tostring(attrs.modification)
    local mimetype = mimetypes.guess(f)
    if mimetype == nil then
      mimetype = "application/x-octet-stream"
    end

    if p_mtime and p_mtime == mtime then
      print("skipping unmodified file " .. f)
      return
    end

    local fb = assert(io.open(f, 'rb'))
    local fb_hash = md5.sumhexa(fb:read('*all'))
    fb:close()

    md5_db:put(fb_hash, f)
    md5_db:put('mtime:' .. f, mtime)
    md5_db:put('mimetype:' .. f, mimetype)
  end
)

md5_db:close()
