import falcon
import hashlib
import json
import mimetypes
import os
import plyvel
import requests
import sys
from threading import Thread


class MichaelJackson(Thread):
    def __init__(self, d, p, initial=False):
        Thread.__init__(self)
        self.d = d
        self.p = p
        self.initial = initial

    def get_md5(self, p):
        if isinstance(p, bytes):
            p = p.decode()
        fr = open(p, 'rb').read()
        return hashlib.md5(fr).hexdigest().encode()

    def get_bday(self, p):
        if isinstance(p, bytes):
            p = p.decode()
        size = str(os.path.getsize(p)).encode()
        mtime = str(os.path.getmtime(p)).encode()
        return b"%s___%s" % (size, mtime)

    def moonwalk(self, root):
        c = 0
        for p in (os.path.join(d, f).encode()
                  for d, _, fl in os.walk(root)
                  for f in fl):

            if p.count(b"/.ldb/"):
                continue

            if self.initial:
                self.d.put(b'PREVIOUS_%d' % c, p)
            else:
                self.d.put(b'CURRENT_%d' % c, p)
            c += 1

            try:
                dbpb = json.loads(self.d.get(p).decode())[0]
            except:
                dbpb = None

            if self.get_bday(p) == dbpb:
                continue

            md5 = self.get_md5(p)
            print(b"MOONWALK: " + p + b" ||| " + md5)
            self.d.put(b'FPATH_' + p, json.dumps([self.get_bday(p).decode(),
                                                  md5.decode()]).encode())
            self.d.put(b'MD5_' + md5, p)

        if not self.initial:
            pre_it = self.d.iterator(prefix=b'PREVIOUS_')
            cur_it = self.d.iterator(prefix=b'CURRENT_')
            pre_list = [p.decode() for _, p in pre_it]
            cur_list = [p.decode() for _, p in cur_it]
            del_fp = list(set(pre_list) - set(cur_list))

            pre_it.seek_to_start()
            for k, _ in pre_it:
                self.d.delete(k)

            with self.d.write_batch() as wb:
                c = 0
                for p in cur_list:
                    wb.put(b'PREVIOUS_%d' % c, p.encode())
                    c += 1

            cur_it.seek_to_start()
            for k, _ in cur_it:
                self.d.delete(k)

            pre_it.close()
            cur_it.close()

            for p in del_fp:
                p = b'FPATH_%s' % p.encode()
                md5 = b'MD5_%s' % json.loads(self.d.get(p).decode())[1].encode()
                self.d.delete(md5)
                self.d.delete(p)

    def run(self):
        self.moonwalk(self.p)


class ManInTheMirror(object):
    def __init__(self, d):
        self.d = d

    def on_get(self, req, resp):
        resp.content_type = 'text/plain'
        resp.accept_ranges = "bytes"
        fpath_it = self.d.iterator(prefix=b'MD5_', include_value=False)
        resp.data = b''.join([k[4:] for k in fpath_it])
        fpath_it.close()


class BillieJean(object):
    def __init__(self, mj, d, p):
        self.mj = mj
        self.d = d
        self.p = p

    def on_get(self, req, resp):
        if self.mj.is_alive():
            resp.data = b'still running in the background...'
        else:
            resp.data = b'moonwalk started again...'
            self.mj = MichaelJackson(self.d, self.p)
            self.mj.start()


class DirtyDiana(object):
    def __init__(self, d):
        self.d = d

    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        fpath_it = self.d.iterator(prefix=b'FPATH_')
        nexthop_it = self.d.iterator(prefix=b'NEXTHOP_')
        fpath_data = b''.join([b''.join([k[6:], b", ", v, b'\n']) for k, v in fpath_it])
        nexthop_data = b''.join([b''.join([v, b", ", k[8:], b'\n']) for k, v in nexthop_it])
        resp.data = fpath_data + nexthop_data
        fpath_it.close()
        nexthop_it.close()


class SmoothCriminal(object):
    def __init__(self, d):
        self.d = d

    def on_get(self, req, resp, md5):
        if md5 == "favicon.ico":
            return
        notafile_path = self.get_path(md5)
        resp.content_type = mimetypes.guess_type(notafile_path)[0]
        resp.accept_ranges = "bytes"
        resp.status = falcon.HTTP_200
        block_size = falcon.API._STREAM_BLOCK_SIZE

        start = 0
        size = os.path.getsize(notafile_path)
        if req.range:
            resp.status = falcon.HTTP_206
            start = req.range[0]
            if req.range[1] and req.range[1] != -1:
                end = req.range[1]
            else:
                end = size - 1
        else:
            end = size - 1

        length = end - start + 1
        resp.content_range = (start, end, size)
        n, d = (length//(block_size), length % (block_size))

        def read_in_chunks(p, start, length, n, d, block_size):
            with open(p, 'rb') as f:
                f.seek(start)
                # yield f.read(length)
                for c in range(n):
                    yield f.read(block_size)
                if d:
                    yield f.read(d)
            return

        resp.stream = read_in_chunks(notafile_path, start, length, n, d, block_size)

    def get_path(self, md5):
        md5 = md5.encode()
        md5_local = b"MD5_" + md5
        path = self.d.get(md5_local)
        if path:
            return path.decode()
        else:
            md5_nh = b"NEXTHOP_" + md5
            path_nh = self.d.get(md5_nh)
            if path_nh:
                path_nh = "http://{}/{}".format(json.loads(path_nh.decode())[0], md5.decode())
                raise falcon.HTTPMovedPermanently(path_nh)
            else:
                raise falcon.HTTPNotFound()


class GetIt(Thread):
    def __init__(self, d, host):
        Thread.__init__(self)
        self.d = d
        self.host = host

    def get_md5s(self):
        r = requests.get("http://{}/dump".format(self.host), stream=True)

        with self.d.write_batch() as wb:
            for chunk in r.iter_content(32):
                nhl = self.d.get(b'NEXTHOP_' + chunk)
                if nhl:
                    nhl = json.loads(nhl.decode())
                    nhl.append(self.host)
                    nhl = list(set(nhl))
                else:
                    nhl = [self.host]
                wb.put(b'NEXTHOP_' + chunk, json.dumps(nhl).encode())

    def run(self):
        self.get_md5s()


class RockinRobin(object):
    def __init__(self, d):
        self.d = d
        self.t = False

    def on_get(self, req, resp, host):
        if self.t and self.t.host == host and self.t.is_alive():
            resp.data = "still harvesting from {}...".format(host)
        else:
            self.t = GetIt(self.d, host)
            self.t.start()


if len(sys.argv) == 2:
    PATH = sys.argv[1]
    print("Serving {}...".format(PATH))
else:
    PATH = "/home/m/Videos"

d = plyvel.DB('{}/.ldb'.format(PATH), create_if_missing=True)
t = MichaelJackson(d, PATH, initial=True)
t.start()

app = api = falcon.API()
api.add_route('/{md5}', SmoothCriminal(d))
api.add_route('/list', DirtyDiana(d))
api.add_route('/dump', ManInTheMirror(d))
api.add_route('/moonwalk', BillieJean(t, d, PATH))
api.add_route('/nexthop/{host}', RockinRobin(d))
# gunicorn -b localhost:7666 notafile:app
# uwsgi --plugins http,python3 --enable-threads --pyargv "/directory/path" --http :7666 --wsgi-file notafile.py --callable app
