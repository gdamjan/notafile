local lfs = require 'lfs'
local ldb = require 'leveldb'

local md5_db = ldb.open(
  'db/md5.db',
  { create_if_missing = true, error_if_exists = false }
)

local path = md5_db:get(ngx.var.file_hash)
local mimetype = md5_db:get('mimetype:' .. path)

ngx.header.content_type = mimetype

local f = assert(io.open(path, "rb"))

local chunk = ""
while true do
  chunk = f:read(4096)
  if chunk == nil then break end

  ngx.print(chunk)
  ngx.flush(true)
end

f:close()
md5_db:close()
